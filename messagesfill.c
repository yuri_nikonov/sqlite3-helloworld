#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sqlite3.h>

#define MAXBUFLEN 16384

int main(int argc, char **argv)
{
    // Describing vars
    FILE *fp;
    char buff[MAXBUFLEN], *datetime, *host, *source, *message, *sql, sqlbuff[MAXBUFLEN], *zErrMsg = NULL;
    const char *tail;
    sqlite3 *db;
    sqlite3_stmt *stmt;
    int rc, linecount = 0, transactionStartedFlag = 0;
    
    clock_t startClock = clock();
    
    // Opening input file
    if ((fp = fopen("messages", "r")) == NULL)
    {
        fprintf(stderr, "Cannot open file.\n");
        return 1;
    }
    
    // Open database
    rc = sqlite3_open("messages.db", &db);
    if (rc)
    {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        return 1;
    }
    else
        fprintf(stdout, "Opened database successfully\n");
    
    // Optimizing options for DB engine
    sqlite3_exec(db, "PRAGMA synchronous = OFF", NULL, 0, &zErrMsg);
    sqlite3_exec(db, "PRAGMA journal_mode = MEMORY", NULL, 0, &zErrMsg);
    
    // Delete table if exists
    rc = sqlite3_exec(db, "drop table if exists messages", NULL, 0, &zErrMsg);
    if ( rc != SQLITE_OK )
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
        fprintf(stdout, "Table dropped successfully\n");
    
    // Creating new table for log data
    sql = "create table messages (id integer primary key autoincrement not null, datetime text, host text, source text, message text);";
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if ( rc != SQLITE_OK )
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
        fprintf(stdout, "New table created successfully\n");
    
    // Preparing query template
    snprintf(sqlbuff, MAXBUFLEN, "insert into messages (datetime, host, source, message) values ( @DAYTIME, @HOST, @SOURCE, @MESSAGE);");
    sqlite3_prepare_v2(db, sqlbuff, -1, &stmt, &tail);
    
    
    // Splitting string to substrings
    datetime = buff;
    while ( fgets(buff, MAXBUFLEN, fp) )
    {
        char *pbuff = buff, *pmess;
        int qcounter = 0;
        
        if (!transactionStartedFlag)
        {
            transactionStartedFlag = 1;
            // Begin transaction for mass iserting
            sqlite3_exec(db, "begin transaction", NULL, 0, &zErrMsg);
        }
        
        // Cutting datetime
        pbuff = memchr(pbuff, 0x20, MAXBUFLEN - ( pbuff - buff )) + 1;
        while ( *pbuff == 0x20 )
            pbuff++;
        pbuff = memchr(pbuff, 0x20, MAXBUFLEN - ( pbuff - buff )) + 1;
        while ( *pbuff == 0x20 )
            pbuff++;
        pbuff = memchr(pbuff, 0x20, MAXBUFLEN - ( pbuff - buff ));
        *pbuff = 0x00;
        pbuff++;
        while ( *pbuff == 0x20 )
            pbuff++;
        
        // Cutting hostname
        host = pbuff;
        pbuff = memchr(pbuff, 0x20, MAXBUFLEN - ( pbuff - buff ));
        *pbuff = 0x00;
        pbuff++;
        
        // Cutting source
        source = pbuff;
        char *sourceend = memchr(pbuff, 0x20, MAXBUFLEN - ( pbuff - buff )), *sourcetemp;
        
        sourcetemp = memchr(pbuff, '[', sourceend - pbuff);
        if (!(sourcetemp = memchr(pbuff, '[', sourceend - pbuff)))
        {
            if (!(sourcetemp = memchr(pbuff, ':', sourceend - pbuff)))
            {
                sourcetemp = sourceend;
            }
        }
        pbuff = sourcetemp;
        *pbuff = 0x00;
        pbuff = sourceend;
        pbuff++;
        
        // Cutting message
        message = pbuff;
        
        // Replacing character \r -> \0
        pbuff = memchr(pbuff, 0x0a, MAXBUFLEN - ( pbuff - buff ));
        *pbuff = 0x00;
        // Assigning text fields into query template
        sqlite3_bind_text(stmt, 1, datetime, -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 2, host, -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 3, source, -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(stmt, 4, message, -1, SQLITE_TRANSIENT);
        
        // Executing query
        sqlite3_step(stmt);
        
        // Cleaning query template
        sqlite3_clear_bindings(stmt);
        sqlite3_reset(stmt);
        linecount++;
        if (!(linecount % 50000))
        {
            transactionStartedFlag = 0;
            sqlite3_exec(db, "end transaction", NULL, 0, &zErrMsg);
            fprintf(stdout, "Processed: %d records\n", linecount);
        }
    }
    
    // Finishing transaction
    if (transactionStartedFlag)
    {
        sqlite3_exec(db, "end transaction", NULL, 0, &zErrMsg);
        fprintf(stdout, "Processed: %d records\n", linecount);
    }
    fprintf(stdout, "Data inserted into table\n");
    
    // Cleaning sqlite3_statement instance
    sqlite3_finalize(stmt);
    
    // Closing DB connection
    sqlite3_close(db);
    fprintf(stdout, "DB connection closed\n");
    double duration = (clock() - startClock) / (double)CLOCKS_PER_SEC;
    fprintf(stdout, "Time duration: %4.3f sec., processed %d records, %4.3f records per sec.\n", duration, linecount, (double)linecount / duration );
    
    // Closing file with input data
    fclose(fp);
    fprintf(stdout, "Input file closed\n");
    
    // End programm
    return 0;
}
